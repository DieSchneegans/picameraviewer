package com.schneegans.picameraviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;

public class ServerSettingActivity extends AppCompatActivity {

    // Private attributes
    private ImageButton mainActivity;
    private EditText currentIpAddress;
    private EditText currentIpPort;
    private EditText port;
    private Spinner resolution;
    private Spinner rotation;
    private SeekBar framerate;
    private LinearLayout networksettings;
    private LinearLayout camerasettings;
    private LinearLayout buttonarea;

    private Toast currentToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_setting);

        port = findViewById(R.id.editText_serverPort);

        resolution = findViewById(R.id.spinner_stream_resolution);

        rotation = findViewById(R.id.spinner_stream_rotation);

        framerate = findViewById(R.id.seekBar_stream_framerate);
        framerate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (currentToast != null)
                    currentToast.cancel();
                currentToast = Toast.makeText(ServerSettingActivity.this, "Value: " + framerate.getProgress(), Toast.LENGTH_SHORT);
                currentToast.show();
            }
        });

        currentIpAddress = findViewById(R.id.editText_currentIPAddress);
        currentIpPort = findViewById(R.id.editText_currentPort);

        networksettings = findViewById(R.id.NetworkSettings);
        camerasettings = findViewById(R.id.StreamSettings);
        buttonarea = findViewById(R.id.ButtonArea);

        // Set default visibility of settings to false
        networksettings.setVisibility(View.INVISIBLE);
        camerasettings.setVisibility(View.INVISIBLE);
        buttonarea.setVisibility(View.INVISIBLE);

        // Framerate seekbar - initialize and load seekbar values
        framerate.setMax(30);

        // Rotation spinner - initialize and load dropdown values
        ArrayAdapter<CharSequence> rotation_adapter = ArrayAdapter.createFromResource(this, R.array.rotation_array, android.R.layout.simple_spinner_dropdown_item);
        rotation.setAdapter(rotation_adapter);

        // Resolution spinner - initialize and load dropdown values
        ArrayAdapter<CharSequence> resolution_adapter = ArrayAdapter.createFromResource(this, R.array.resolution_array, android.R.layout.simple_spinner_dropdown_item);
        resolution.setAdapter(resolution_adapter);

        // Load last user data
        SharedPreferences userData = getSharedPreferences("ServerSettingsActivityData", 0);

        currentIpAddress.setText(userData.getString("currentIpAddress", ""));
        currentIpPort.setText(userData.getString("currentIpPort", ""));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Wrote user data to shared preferences
        SharedPreferences userData = getSharedPreferences("ServerSettingsActivityData", 0);
        SharedPreferences.Editor editor = userData.edit();

        editor.putString("currentIpAddress", currentIpAddress.getText().toString());
        editor.putString("currentIpPort", currentIpPort.getText().toString());

        // Save user data
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void openMainActivity(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void testConnection(View view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://" + currentIpAddress.getText().toString() + ":" + currentIpPort.getText().toString() + "/api/connection";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (currentToast != null)
                            currentToast.cancel();
                        currentToast = Toast.makeText(ServerSettingActivity.this, "Connected...", Toast.LENGTH_SHORT);
                        currentToast.show();

                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = jsonArray.getJSONObject(0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            // Set server port value
                            port.setText(jsonObject.getString("network_port"));

                            // Set server resolution value
                            String json_resolution = jsonObject.getString("camera_resolution");
                            for (int i = 0; i < resolution.getAdapter().getCount(); i++) {
                                String spinnerResolution = resolution.getAdapter().getItem(i).toString();
                                if (spinnerResolution.equals(json_resolution)) {
                                    resolution.setSelection(i);
                                    break;
                                }
                            }

                            // Set server framerate value
                            framerate.setProgress(jsonObject.getInt("camera_framerate"));

                            // Set server rotation value
                            String json_rotation = jsonObject.getString("camera_rotation");
                            for (int i = 0; i < rotation.getAdapter().getCount(); i++) {
                                String test = rotation.getAdapter().getItem(i).toString();
                                if (test.equals(json_rotation)) {
                                    rotation.setSelection(i);
                                    break;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        networksettings.setVisibility(View.VISIBLE);
                        camerasettings.setVisibility(View.VISIBLE);
                        buttonarea.setVisibility(View.VISIBLE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (currentToast != null)
                    currentToast.cancel();
                currentToast = Toast.makeText(ServerSettingActivity.this, "No connection...", Toast.LENGTH_SHORT);
                currentToast.show();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void restartServer(View view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

        // Generate url
        String apiSettings = "";
        apiSettings += "resolution=" + resolution.getSelectedItem().toString();
        apiSettings += "&framerate=" + framerate.getProgress();
        apiSettings += "&rotation=" + rotation.getSelectedItem().toString();
        if(!port.getText().toString().equals("")) {
            apiSettings += "&port=" + port.getText().toString();
        }

        String url = "http://" + currentIpAddress.getText().toString() + ":" + currentIpPort.getText().toString() + "/api/settings?" + apiSettings;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (currentToast != null)
                            currentToast.cancel();
                        currentToast = Toast.makeText(ServerSettingActivity.this, "Connected...", Toast.LENGTH_SHORT);
                        currentToast.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (currentToast != null)
                    currentToast.cancel();
                currentToast = Toast.makeText(ServerSettingActivity.this, "Reload the stream...", Toast.LENGTH_SHORT);
                currentToast.show();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}