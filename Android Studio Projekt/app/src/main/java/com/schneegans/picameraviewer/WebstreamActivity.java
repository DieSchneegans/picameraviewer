package com.schneegans.picameraviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.longdo.mjpegviewer.MjpegView;

public class WebstreamActivity extends AppCompatActivity {

    // private Attribute
    private MjpegView webstream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webstream);
        Intent webstream_intent = getIntent();

        webstream = findViewById(R.id.webstream_viewer);

        // read parameter
        String ipAddress = webstream_intent.getStringExtra("IpAddress");
        int ipPort = webstream_intent.getIntExtra("IpPort", 8000);
        int rotation = webstream_intent.getIntExtra("rotation", 0);

        setupStream(ipAddress, ipPort, rotation);
    }

    @Override
    protected void onResume() {
        webstream.startStream();
        super.onResume();
    }

    @Override
    protected void onPause() {
        webstream.stopStream();
        super.onPause();
    }

    @Override
    protected void onStop() {
        webstream.stopStream();
        super.onStop();
    }

    private void setupStream(String ipAddress, int ipPort, int rotation) {
        webstream.setUrl("http://" + ipAddress + ":" + String.valueOf(ipPort) + "/stream.mjpg");
        webstream.setRotation(rotation);

        webstream.setAdjustHeight(true);
        webstream.setMode(MjpegView.MODE_FIT_WIDTH);
        webstream.invalidate();
    }
}