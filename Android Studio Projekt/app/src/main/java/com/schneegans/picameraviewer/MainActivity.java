package com.schneegans.picameraviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    // private Attributes
    private ToggleButton connectionMode;
    private LinearLayout layout_ipAddress;
    private EditText editText_url;
    private EditText ipAddress1;
    private EditText ipAddress2;
    private EditText ipAddress3;
    private EditText ipAddress4;
    private EditText ipPort;
    private Spinner rotation;

    private static int quality_steps = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectionMode = findViewById(R.id.toggleButton_connectionMode);
        connectionMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layout_ipAddress.setVisibility(View.INVISIBLE);
                    editText_url.setVisibility(View.VISIBLE);
                } else {
                    layout_ipAddress.setVisibility(View.VISIBLE);
                    editText_url.setVisibility(View.INVISIBLE);
                }
            }
        });

        layout_ipAddress = findViewById(R.id.layout_ipAddress);
        editText_url = findViewById(R.id.editText_URL);

        ipAddress1 = findViewById(R.id.editText_ipAddress1);
        ipAddress2 = findViewById(R.id.editText_ipAddress2);
        ipAddress3 = findViewById(R.id.editText_ipAddress3);
        ipAddress4 = findViewById(R.id.editText_ipAddress4);
        ipPort = findViewById(R.id.editText_ipPort);

        // Rotation spinner - initialize and load dropdown values
        rotation = findViewById(R.id.spinner_rotation);
        ArrayAdapter<CharSequence> rotation_adapter = ArrayAdapter.createFromResource(this, R.array.rotation_array, android.R.layout.simple_spinner_dropdown_item);
        rotation.setAdapter(rotation_adapter);

        // Load last user data
        SharedPreferences userData = getSharedPreferences("MainActivityData", 0);

        connectionMode.setChecked(userData.getBoolean("connectionMode", Boolean.FALSE));
        if (connectionMode.isChecked()) {
            layout_ipAddress.setVisibility(View.INVISIBLE);
            editText_url.setVisibility(View.VISIBLE);
        } else {
            layout_ipAddress.setVisibility(View.VISIBLE);
            editText_url.setVisibility(View.INVISIBLE);
        }
        ipAddress1.setText(userData.getString("ipAddress1", ""));
        ipAddress2.setText(userData.getString("ipAddress2", ""));
        ipAddress3.setText(userData.getString("ipAddress3", ""));
        ipAddress4.setText(userData.getString("ipAddress4", ""));
        editText_url.setText(userData.getString("url", "picamera"));
        ipPort.setText(userData.getString("ipPort", "8000"));
        rotation.setSelection(userData.getInt("clientRotation", 0));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Wrote user data to shared preferences
        SharedPreferences userData = getSharedPreferences("MainActivityData", 0);
        SharedPreferences.Editor editor = userData.edit();

        editor.putBoolean("connectionMode", connectionMode.isChecked());
        editor.putString("ipAddress1", ipAddress1.getText().toString());
        editor.putString("ipAddress2", ipAddress2.getText().toString());
        editor.putString("ipAddress3", ipAddress3.getText().toString());
        editor.putString("ipAddress4", ipAddress4.getText().toString());
        editor.putString("url", editText_url.getText().toString());
        editor.putString("ipPort", ipPort.getText().toString());
        editor.putInt("clientRotation", rotation.getSelectedItemPosition());

        // Save user data
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void openServerSettings(View view) {
        Intent serverSettings_intent = new Intent(this, ServerSettingActivity.class);
        startActivity(serverSettings_intent);
    }

    public void startView(View view) {
        Intent webstream_intent = new Intent(this, WebstreamActivity.class);
        if (connectionMode.isChecked()) {
            webstream_intent.putExtra("IpAddress", readUrlFromActivity(view));
        } else {
            webstream_intent.putExtra("IpAddress", readIpAddressFromActivity(view));
        }
        webstream_intent.putExtra("IpPort", readIpPortFromActivity(view));
        webstream_intent.putExtra("rotation", readRotationFromActivity(view));
        startActivity(webstream_intent);
    }

    private String readIpAddressFromActivity(View view) {
        String ipAdresse1_txt = ipAddress1.getText().toString();
        String ipAdresse2_txt = ipAddress2.getText().toString();
        String ipAdresse3_txt = ipAddress3.getText().toString();
        String ipAdresse4_txt = ipAddress4.getText().toString();
        return ipAdresse1_txt + "." + ipAdresse2_txt + "." + ipAdresse3_txt + "." + ipAdresse4_txt;
    }

    private String readUrlFromActivity(View view) {
        return editText_url.getText().toString();
    }

    private int readIpPortFromActivity(View view) {
        return Integer.valueOf(ipPort.getText().toString());
    }

    private int readRotationFromActivity(View view) {
        return Integer.valueOf(rotation.getSelectedItem().toString());
    }
}