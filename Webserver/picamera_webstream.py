# Web streaming example
# Source code from the official PiCamera package
# http://picamera.readthedocs.io/en/latest/recipes2.html#web-streaming

import io
import picamera
import logging
import os
import json
import socketserver
import subprocess
import sys
import socket
from urllib.parse import urlparse
import configparser
from threading import Condition
from http import server

PAGE="""\
<html>
<head>
<title>PiCameraViewer by Schneegans</title>
</head>
<body>
<center><h1>PiCameraViewer by Schneegans</h1></center>
<center><img src="stream.mjpg" width="auto" height="90%"></center>
</body>
</html>
"""

class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == "/api/debug/reboot":
            command = "/usr/bin/sudo /sbin/reboot now"
            process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
            processOutput = process.communicate()[0]
            print(processOutput)
        elif self.path == "/api/connection":
            _g_posts = [
                {
                    'network_port': config["network"]["port"],
                    'camera_resolution': config["camera"]["resolution"],
                    'camera_framerate': config["camera"]["framerate"],
                    'camera_rotation': config["camera"]["rotation"],
                }
            ]
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps(_g_posts).encode('utf-8'))
        elif self.path.split("?")[0] == "/api/settings":
            query = urlparse(self.path).query
            logging.warning(query)
            query_components = dict(qc.split("=") for qc in query.split("&"))
            logging.warning(query_components)
            try:
                resolution = query_components["resolution"]
                config["camera"]["resolution"] = resolution
            except:
                logging.warning("No resolution in uri")
            try:
                rotation = query_components["rotation"]
                config["camera"]["rotation"] = rotation
            except:
                logging.warning("No rotation in uri")
            try:
                framerate = query_components["framerate"]
                config["camera"]["framerate"] = framerate
            except:
                logging.warning("No framerate in uri")
            try:
                port = query_components["port"]
                config["network"]["port"] = port
            except:
                logging.warning("No port in uri")

            with open('config.conf', 'w') as configfile:
                config.write(configfile)
                server.shutdown()

        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()

class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


config = configparser.ConfigParser()
config.read("config.conf")

with picamera.PiCamera(resolution=config["camera"]["resolution"], framerate=config["camera"].getint("framerate")) as camera:
    output = StreamingOutput()
    camera.rotation = config["camera"].getint("rotation")
    camera.start_recording(output, format='mjpeg')
    try:
        address = ('', config["network"].getint("port"))
        server = StreamingServer(address, StreamingHandler)
        server.serve_forever()
    finally:
        camera.stop_recording()